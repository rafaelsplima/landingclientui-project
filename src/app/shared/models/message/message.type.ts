export enum MessageType {
    Logon = 'Logon',
    Subscribe = 'Subscribe',
    Unsubscribe = 'Unsubscribe',

    Subscribed = 'Subscribed',

    OpenOrder = 'OpenOrder',
    CancelOrder = 'CancelOrder',
    OpenOrderConfirmation = 'OpenOrderConfirmation',
    CancelOrderConfirmation = 'CancelOrderConfirmation',
    OrderError = 'OrderError',

    DepositConfirmation = 'DepositConfirmation',
    WithdrawalConfirmation = 'WithdrawalConfirmation',

    NewOrder = 'NewOrder',
    Trade = 'Trade',
    ExecutionReport = 'ExecutionReport',

    ExecutionMaker = 'ExecutionMaker',
    ExecutionTaker = 'ExecutionTaker',

    BalanceChanged = 'BalanceChanged',

    Transfer = 'Transfer', // Withdrawal or Deposit
    ClientDepositConfirmed = 'ClientDepositConfirmed',
    ClientWithdrawalConfirmed = 'ClientWithdrawalConfirmed',

    Ticker = 'Ticker',
    UserLoggedIn = 'UserLoggedIn',

    NewAddress = 'NewAddress',
    CreateNewAddress = 'NewAccount',
    RemoveAccount = 'RemoveAccount',

    PendingOperation = 'PendingOperation',
    UserAttributesUpdate = 'UserAttributesUpdate'

}
