export enum OrderSide {
    Buy = 'Buy',
    Sell = 'Sell',
    AllTO = 'AllTO'
}
