export enum TransferStatus {
    New = 'new',
    Approved = 'Approved',
    Rejected = 'Rejected',
    AllStatus = 'AllStatus'
}
