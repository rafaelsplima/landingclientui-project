import { TransferStatus } from './transfer.status';
import { TransferType } from './transfer.type';

export class Transfer {
    transferId?: string;
    symbol: string;
    amount: number;
    status: TransferStatus;
    time: number;
    type: TransferType;
}
