import { Pipe, PipeTransform } from '@angular/core';
@Pipe({name: 'replaceTwoDots'})
export class ReplaceTwoDots implements PipeTransform {
  transform(value: string): string {
    return value.replace(':', ' > ');
  }
}
