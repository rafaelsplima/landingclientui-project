import { Component, OnInit } from '@angular/core';
import { environment } from '../../../../environments/environment';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss']
})

export class NavbarComponent implements OnInit {

    public production;
    public navbarMobile;
    public signUpUrl;
    public signInUrl;

    constructor() {
        this.production = environment.production;
        this.signUpUrl = environment.signup_url;
        this.signInUrl = environment.signin_url;
    }

    ngOnInit() {
        this.navbarMobile = true;
    }

    public toggleNavbarMobile(): void{
        this.navbarMobile = !this.navbarMobile;
    }

}
