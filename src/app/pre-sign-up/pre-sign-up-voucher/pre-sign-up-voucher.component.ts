import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
    selector: 'app-pre-sign-up-voucher',
    templateUrl: './pre-sign-up-voucher.component.html',
    styleUrls: ['./pre-sign-up-voucher.component.scss']
})
export class PreSignUpVoucherComponent implements OnInit {

    public form: FormGroup;
    public voucher: string;

    constructor(
        private router: Router,
    ) { }

    ngOnInit() {
        this.form = new FormGroup({
            'voucher': new FormControl(this.voucher, Validators.required)
        });
    }

    public validateVoucher(): void {
        this.router.navigate(['novo-cadastro/passo-1']);
    }

}
