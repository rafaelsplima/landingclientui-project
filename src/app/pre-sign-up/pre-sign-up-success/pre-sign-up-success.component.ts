import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-pre-sign-up-success',
    templateUrl: './pre-sign-up-success.component.html',
    styleUrls: ['./pre-sign-up-success.component.scss']
})
export class PreSignUpSuccessComponent implements OnInit {

    constructor() { }

    ngOnInit() {
    }

}
