import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AppStore } from '../../app.store';

@Component({
    selector: 'app-pre-sign-up-login',
    templateUrl: './pre-sign-up-login.component.html',
    styleUrls: ['./pre-sign-up-login.component.scss']
})
export class PreSignUpLoginComponent implements OnInit {

    public login: FormGroup;
    public email: string;

    constructor(
        @Inject(AppStore) private store,
    ) { }

    ngOnInit() {
        const store = this.store.getState().toJS().signUp;
        this.login = new FormGroup({
            'email': new FormControl(this.email, [Validators.required, Validators.email]),
        });
    }

    public verifyData(): void {

    }

}
