import { Component, OnInit, Inject } from '@angular/core';
import { AppStore } from '../../app.store';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { TermsModalComponent } from '../../shared/modals/terms-modal/terms-modal.component';
import { PrivacyModalComponent } from '../../shared/modals/privacy-modal/privacy-modal.component';
import { ErrorModalComponent } from '../../shared/modals/error-modal/error-modal.component';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Component({
    selector: 'app-pre-sign-up-form',
    templateUrl: './pre-sign-up-form.component.html',
    styleUrls: ['./pre-sign-up-form.component.scss']
})
export class PreSignUpFormComponent implements OnInit {

    public form: FormGroup;
    public userName: string;
    public email: string;
    public acceptTerms: boolean;

    constructor(
        @Inject(AppStore) private store,
        private router: Router,
        private dialog: MatDialog,
        private http: HttpClient
    ) { }

    ngOnInit() {
        this.acceptTerms = false;
        const store = this.store.getState().toJS().signUp;
        this.userName = store.name;
        this.form = new FormGroup({
            'name': new FormControl(this.userName, Validators.required),
            'email': new FormControl(this.email, [Validators.required, Validators.email]),
        });
    }

    public verifyData(): void {
        if (!this.acceptTerms) {
            this.openModalError('Leia e aceite os termos de uso e política de privacidade!');
        } else if (this.form.controls.email.status === 'VALID'
            && this.form.value.name !== '' && this.form.value.lastName !== '') {

            const postData = {
                email: this.form.value.email
            };

            const req = this.http.post('https://h91z80vnpd.execute-api.us-east-1.amazonaws.com/v1/waiting-list', postData, {
                headers: new HttpHeaders().set('Content-Type', 'application/json')
            });

            req.subscribe(data => {
                this.router.navigate(['pre-cadastro/sucesso']);

            }, err => {
            });

            this.router.navigate(['pre-cadastro/sucesso']);

        }
    }

    public keytab(event): void {
        const element = event.srcElement.id;
        if (element === 'name') {
            document.getElementById('email').focus();
        } else if (element === 'email') {
            this.verifyData();
        }
    }

    public openModalError(msg: string): void {
        this.dialog.open(ErrorModalComponent, {
            data: {
                msg: msg
            }
        });
    }

    public openTermsDialog(): void {
        this.dialog.open(TermsModalComponent, {
            autoFocus: true,
            panelClass: 'sign-error-dialog',
            hasBackdrop: true,
            backdropClass: 'white-backdrop',
        });
    }

    public openPrivacyDialog(): void {
        this.dialog.open(PrivacyModalComponent, {
            autoFocus: true,
            panelClass: 'sign-error-dialog',
            hasBackdrop: true,
            backdropClass: 'white-backdrop',
        });
    }
}
