import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-vacancy',
    templateUrl: './vacancy.component.html',
    styleUrls: ['./vacancy.component.scss']
})
export class VacancyComponent implements OnInit {
    public vacancy;

    constructor(
        private route: ActivatedRoute,
    ) { }

    ngOnInit() {
        this.vacancy = this.route.snapshot.params.id;
    }

}
