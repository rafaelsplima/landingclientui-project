import { Component, OnInit, ElementRef, Inject } from '@angular/core';
import { Auth0Service } from '../auth/auth0.service';
import { MktDataService } from '../../api/mktdata.service';

@Component({
    selector: 'app-home-not-logged',
    templateUrl: './home-not-logged.component.html',
    styleUrls: ['./home-not-logged.component.scss']
})
export class HomeNotLoggedComponent implements OnInit {
    public orderDialogVisible = 'hidden';
    public withdepDialogVisible = 'hidden';
    public profileDialogVisible = 'hidden';
    public menuMobile;
    public sidebarMode = 'full';

    public loggedIn = false;

    constructor(
        @Inject(Auth0Service) public auth: Auth0Service,
        private elementRef: ElementRef,
        @Inject(MktDataService) private mktDataService: MktDataService,
    ) { }

    ngOnInit() {
        localStorage.setItem('sidebarMode', 'full');
        this.elementRef.nativeElement.ownerDocument.body.style.overflow = 'hidden';
        this.mktDataService.subscribe('ETH:BRL', false);
    }

    public refresh(): void{
        location.reload();
    }

    public toggleSidebar(): void {
        if (this.sidebarMode === 'full') {
            this.sidebarMode = 'collapse';
            localStorage.setItem('sidebarMode', 'collapse');
        } else {
            this.sidebarMode = 'full';
            localStorage.setItem('sidebarMode', 'full');
        }
    }

    public onMenuMobileEvent(evento): void {
        this.menuMobile = evento.v;
    }

}
