import { Component, Inject } from '@angular/core';
import { Auth0Service } from './auth/auth0.service';
import { TranslateService } from '@ngx-translate/core';
import { Keepalive } from '@ng-idle/keepalive';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    public translationsLoaded = false;

    public constructor(
        private translate: TranslateService,
        @Inject(Auth0Service) public auth: Auth0Service,
        private keepAlive: Keepalive
    ) {
        this.language();
        this.keepAlive.interval(15);
        this.keepAlive.onPing.subscribe(() => { });
    }

    private language() {
        try {
            this.translate.addLangs(['pt-br']);
            this.translate.setDefaultLang('pt-br');

            const updateTranslationsLoaded = _ => this.translationsLoaded = true;
            this.translate.use('pt-br')
                .subscribe(updateTranslationsLoaded, updateTranslationsLoaded);
        } catch (e) {
            console.warn(e);
        }
    }
}
