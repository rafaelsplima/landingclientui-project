import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { AmChartsService, AmChart } from '@amcharts/amcharts3-angular';
import { AppStore } from '../../../app.store';
import { DepthChartActions } from '../../../app.actions';
import { DEPTHCHART_RENDER_INTERVAL } from '../../../app.constants';

@Component({
    selector: 'app-depthchart',
    templateUrl: './depthchart.component.html',
    styleUrls: ['./depthchart.component.scss']
})
export class DepthchartComponent implements OnDestroy, OnInit {
    public chart: AmChart;
    public chartData = [];
    public counter;

    constructor(
        private AmCharts: AmChartsService,
        @Inject(AppStore) private store,
    ) {
        store.subscribe(() => {
            const state = this.store.getState().toJS();
        });
    }

    ngOnDestroy() {
        if (this.chart) {
            this.AmCharts.destroyChart(this.chart);
        }
        clearTimeout(this.counter);
    }

    ngOnInit() {
        this.updateChartData();
        this.createChartOptions();
        this.timer();
    }

    public createChartOptions(): void {
        this.chart = this.AmCharts.makeChart('depthchartdiv', {
            'type': 'serial',
            'hideCredits': true,
            'theme': 'dark',
            'dataProvider': this.chartData,
            'fontFamily': 'Montserrat',
            'decimalSeparator': ',',
            'thousandsSeparator': '.',
            'graphs': [
                {
                    'id': 'bids',
                    'fillAlphas': 0.1,
                    'lineAlpha': 1,
                    'lineThickness': 2,
                    'lineColor': '#4dde90',
                    'type': 'step',
                    'valueField': 'bidstotalvolume',
                    'balloonFunction': this.balloon,
                },
                {
                    'id': 'asks',
                    'fillAlphas': 0.1,
                    'lineAlpha': 1,
                    'lineThickness': 2,
                    'lineColor': '#ff4823',
                    'type': 'step',
                    'valueField': 'askstotalvolume',
                    'balloonFunction': this.balloon,
                }
            ],
            'chartCursor': {
                'oneBalloonOnly': true,
            },
            'categoryField': 'value',
            'valueAxes': [
                {
                    'position': 'right',
                    'title': 'Volume'
                }
            ],
            'categoryAxis': {
                'title': 'Price',
                'minHorizontalGap': 100,
                'startOnAxis': true,
                'showFirstLabel': false,
                'showLastLabel': false
            }
        });
    }

    public updateChartData(): void {
        const pair = this.store.getState().get('currentTicker').toJS().pair;

        const bids = this.store.getState().getIn(['orderbook', 'bid']).sortBy((order) => -order.price)
            .toList().toArray().filter((order) => order.symbol === pair);
        const asks = this.store.getState().getIn(['orderbook', 'ask']).sortBy((order) => -order.price)
            .toList().toArray().filter((order) => order.symbol === pair);

        asks.reverse();

        const totalAsksAmount = asks.reduce(function (accumulator, value, index, array) {
            asks[index].lastVolume = accumulator + parseFloat(value.amount);
            return accumulator + parseFloat(value.amount);
        }, 0.0);

        const totalBidsAmount = bids.reduce(function (accumulator, value, index, array) {
            bids[index].lastVolume = accumulator + parseFloat(value.amount);
            return accumulator + parseFloat(value.amount);
        }, 0.0);

        const data = [];

        bids.reverse();

        bids.forEach(bid => {
            data.push(
                {
                    bidstotalvolume: bid.lastVolume,
                    bidsvolume: bid.amount,
                    value: bid.price
                }
            );
        });
        asks.forEach(ask => {
            data.push(
                {
                    askstotalvolume: ask.lastVolume,
                    asksvolume: ask.amount,
                    value: ask.price
                }
            );
        });

        this.store.dispatch(DepthChartActions.resetDepthChart());
        this.store.dispatch(DepthChartActions.loadDepthchart(data));
        this.chartData = [];
        this.chartData = this.store.getState().get('depthchart').toJS();
    }

    public timer(): void {
        this.counter = setTimeout(() => {
            this.updateChartData();
            this.createChartOptions();
            this.timer();
        }, DEPTHCHART_RENDER_INTERVAL);
    }

    public balloon(item): string {
        let txt;
        if (item.graph.id === 'asks') {
            txt = 'Ask: <strong>' + item.dataContext.value + '</strong><br/>'
                + 'Total volume: <strong>' + item.dataContext.askstotalvolume + '</strong><br/>'
                + 'Volume: <strong>' + item.dataContext.asksvolume + '</strong>';
        } else {
            txt = 'Bid: <strong>' + item.dataContext.value + '</strong><br/>'
                + 'Total volume: <strong>' + item.dataContext.bidstotalvolume + '</strong><br/>'
                + 'Volume: <strong>' + item.dataContext.bidsvolume + '</strong>';
        }
        return txt;
    }

}
