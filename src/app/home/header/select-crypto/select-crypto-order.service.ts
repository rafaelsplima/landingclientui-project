import { Injectable, EventEmitter } from '@angular/core';

@Injectable()
export class SelectCryptoOrderService {
    static changeValueOrder: EventEmitter<any> = new EventEmitter<any>();

    constructor() {
    }

    public onChangedOrder(matSelectChange): void {
        SelectCryptoOrderService.changeValueOrder.emit(matSelectChange);
    }
}
