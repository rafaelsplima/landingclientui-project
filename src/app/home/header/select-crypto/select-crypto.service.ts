import { Injectable, EventEmitter } from '@angular/core';

@Injectable()
export class SelectCryptoService {

    static changeValue: EventEmitter<any> = new EventEmitter<any>();

    constructor() {
    }

    public onChanged(matSelectChange): void {
        SelectCryptoService.changeValue.emit(matSelectChange);
    }
}
