import { Component, Inject } from '@angular/core';
import { MktDataService } from '../../../../api/mktdata.service';
import { AppStore } from '../../../app.store';

@Component({
    selector: 'app-ticker',
    templateUrl: './ticker.component.html',
    styleUrls: ['./ticker.component.scss']
})
export class TickerComponent {
    public currentPair: string;
    public currentCryptoSymbol: string;
    public loggedIn = false;

    public message;

    constructor(
        @Inject(MktDataService) private mktDataService: MktDataService,
        @Inject(AppStore) private store
    ) {
    }

    public logout(): void {
        const state = this.store.getState().toJS();
        this.currentPair = this.store.getState().get('currentTicker').toJS().pair;
        this.mktDataService.subscribe(this.currentPair, false);
    }
}
