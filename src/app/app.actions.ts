export interface Action {
    type: string;
    payload?: any;
}

export const SAVE_PROFILE = 'saveProfile';
export const SAVE_AUTH = 'saveAuth';
export const CREATE_SIGNUP = 'createSignUp';
export const MY_ORDER = 'myOrder';
export const NEW_PAIR = 'newPair';
export const ALLOW_PAIR = 'allowPair';
export const UPDATE_PAIR_INFO = 'updatePairInfo';
export const UPDATE_PAIR_LASTPX = 'updatePairLastPx';
export const RCVD_NEW_ORDER = 'rcvdNewOrder';
export const RCVD_CANCEL_ORDER = 'rcvdCancelOrder';
export const EXECUTION_REPORT = 'executionReport';
export const SELECT_PAIR = 'currentTicker';
export const LOAD_CANDLECHART = 'loadCandlechart';
export const UPDATE_CANDLECHART = 'updateCandlechart';
export const UPDATE_FIRST_ITEM_CANDLECHART = 'updateFirstItemCandlechart';
export const CREATE_FILTERED_CANDLECHART = 'createFilteredCandlechart';
export const RESET_FILTERED_CANDLECHART = 'resetFilteredCandlechart';
export const RESET_DEPTHCHART = 'resetDepthchart';
export const LOAD_DEPTHCHART = 'loadDepthchart';
export const LOG_IN = 'logIn';
export const LOG_OUT = 'logOut';
export const NEW_MESSAGE = 'newMessage';

export class ProfileActions {
    static saveProfile(profile: any): Action {
        return {
            type: SAVE_PROFILE,
            payload: profile
        };
    }
    static saveAuth(accessToken: string, idToken: string, expiresAt: string): Action {
        return {
            type: SAVE_AUTH,
            payload: {
                accessToken: accessToken,
                idToken: idToken,
                expiresAt: expiresAt
            }
        };
    }
    static createSignUp(
        name: string,
        cpf: string,
        birthday: string,
        email: string,
        phone: string,
        cel: string,
        city: string,
        state: string,
        address: string,
        number: number,
        complement: string,
        zipCode: string,
        neightborhood: string,
        mother: string,
        password: string
    ): Action {
        return {
            type: CREATE_SIGNUP,
            payload: {
                name,
                cpf,
                birthday,
                email,
                phone,
                cel,
                city,
                state,
                address,
                number,
                complement,
                zipCode,
                neightborhood,
                mother,
                password
            }
        };
    }
}

export class UserActions {
    static logIn(): Action {
        return {
            type: LOG_IN
        };
    }
    static logOut(): Action {
        return {
            type: LOG_OUT
        };
    }
}

export class TickerActions {
    static currentTicker(pair: string): Action {
        return {
            type: SELECT_PAIR,
            payload: {
                pair
            }
        };
    }
    static newPair(pair: string): Action {
        return {
            type: NEW_PAIR,
            payload: {
                pair
            }
        };
    }
    static allowPair(pair: string): Action {
        return {
            type: ALLOW_PAIR,
            payload: {
                pair
            }
        };
    }
}

export class MktDataActions {
    static updatePairInfo(pair: string, last: number, volume: number, variation: number): Action {
        return {
            type: UPDATE_PAIR_INFO,
            payload: {
                pair,
                last,
                variation,
                volume
            }
        };
    }
    static updatePairLastPx(pair: string, last: number): Action {
        return {
            type: UPDATE_PAIR_LASTPX,
            payload: {
                pair,
                last
            }
        };
    }
    static rcvdNewOrder(orderId, symbol, side, amount, price) {
        return {
            type: RCVD_NEW_ORDER,
            payload: {
                orderId: orderId,
                symbol: symbol,
                side: side,
                amount: amount,
                price: price
            }
        };
    }
    static rcvdCancelOrder(orderId, symbol, side, amount, price) {
        return {
            type: RCVD_CANCEL_ORDER,
            payload: {
                orderId: orderId,
                symbol: symbol,
                side: side,
                amount: amount,
                price: price
            }
        };
    }
    static executionReport(size, symbol, price, time) {
        return {
            type: EXECUTION_REPORT,
            payload: {
                size: size,
                symbol: symbol,
                price: price,
                time: time
            }
        };
    }
}

export class OrderActions {
    static myOrder(orderId, pair, side, status, size, price, filled, avgPrice, time, externalOrderId): Action {
        return {
            type: MY_ORDER,
            payload: {
                orderId,
                pair,
                side,
                status,
                size,
                price,
                filled,
                avgPrice,
                time,
                externalOrderId
            }
        };
    }
}

export class CandleChartActions {
    static loadCandlechart(
        date: Date,
        l: number,
        h: number,
        o: number,
        c: number,
        pair: string
    ): Action {
        return {
            type: LOAD_CANDLECHART,
            payload: [
                {
                    date,
                    l,
                    h,
                    o,
                    c,
                    pair
                }
            ]
        };
    }
    static updateCandlechart(
        date: Date,
        l: number,
        h: number,
        o: number,
        c: number,
        pair: string
    ): Action {
        return {
            type: UPDATE_CANDLECHART,
            payload: {
                date,
                l,
                h,
                o,
                c,
                pair
            }
        };
    }
    static updateFirstItemCandlechart(
        date: Date,
        l: number,
        h: number,
        o: number,
        c: number,
        pair: string
    ): Action {
        return {
            type: UPDATE_FIRST_ITEM_CANDLECHART,
            payload: {
                date,
                l,
                h,
                o,
                c,
                pair
            }
        };
    }
}

export class FilteredCandlechart {
    static createFilteredCandlechart(filteredCandlecChartData: any): Action {
        return {
            type: CREATE_FILTERED_CANDLECHART,
            payload: filteredCandlecChartData
        };
    }
    static resetFilteredCandlechart(): Action {
        return {
            type: RESET_FILTERED_CANDLECHART,
            payload: []
        };
    }
}

export class DepthChartActions {
    static resetDepthChart(): Action {
        return {
            type: RESET_DEPTHCHART,
            payload: []
        };
    }
    static loadDepthchart(depthChartData: any): Action {
        return {
            type: LOAD_DEPTHCHART,
            payload: depthChartData
        };
    }
}

export class MessageActions {
    static newMessage(payload): Action {
        return {
            type: NEW_MESSAGE,
            payload
        };
    }
}
