import { Component, OnInit, Inject, Output, AfterViewInit } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { AppStore } from '../../app.store';
import { ProfileActions } from '../../app.actions';
import { Router } from '@angular/router';

@Component({
    selector: 'app-sign-up-step-3',
    templateUrl: './sign-up-step-3.component.html',
    styleUrls: ['./sign-up-step-3.component.scss']
})
export class SignUpStep3Component implements OnInit, AfterViewInit {

    public step3: FormGroup;
    public email: string;

    public fills = ['', '', ''];
    public blanks = ['', '', '', '', ''];

    constructor(
        @Inject(AppStore) private store,
        private router: Router,
    ) { }

    ngAfterViewInit() {
        document.getElementById('email').focus();
    }

    ngOnInit() {
        const store = this.store.getState().toJS().signUp;
        this.email = store.email;
        this.step3 = new FormGroup({
            'email': new FormControl(this.email, [Validators.required, Validators.email]),
        });
    }

    public createSignUp(): void {
        this.store.dispatch(ProfileActions.createSignUp(
            this.store.getState().toJS().signUp.name,
            this.store.getState().toJS().signUp.cpf,
            this.store.getState().toJS().signUp.birthday,
            this.email,
            this.store.getState().toJS().signUp.phone,
            this.store.getState().toJS().signUp.cel,
            this.store.getState().toJS().signUp.city,
            this.store.getState().toJS().signUp.state,
            this.store.getState().toJS().signUp.address,
            this.store.getState().toJS().signUp.number,
            this.store.getState().toJS().signUp.complement,
            this.store.getState().toJS().signUp.zipCode,
            this.store.getState().toJS().signUp.neightborhood,
            this.store.getState().toJS().signUp.mother,
            this.store.getState().toJS().signUp.password
        ));
        this.router.navigate(['/novo-cadastro/passo-4']);
    }

    public keytab(event): void {
        if (this.step3.controls.email.status === 'VALID') {
            this.createSignUp();
        }
    }

}
