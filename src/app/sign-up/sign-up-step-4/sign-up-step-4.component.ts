import { Component, OnInit, Inject, AfterViewInit } from '@angular/core';
import { AppStore } from '../../app.store';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ProfileActions } from '../../app.actions';
import { Router } from '@angular/router';

@Component({
    selector: 'app-sign-up-step-4',
    templateUrl: './sign-up-step-4.component.html',
    styleUrls: ['./sign-up-step-4.component.scss']
})
export class SignUpStep4Component implements OnInit, AfterViewInit {
    public step4: FormGroup;
    public phone: string;
    public cel: string;

    public fills = ['', '', '', ''];
    public blanks = ['', '', '', ''];

    constructor(
        @Inject(AppStore) private store,
        private router: Router,
    ) { }

    ngAfterViewInit() {
        document.getElementById('phone').focus();
    }

    ngOnInit() {
        const store = this.store.getState().toJS().signUp;
        this.phone = store.phone;
        this.cel = store.cel;
        this.step4 = new FormGroup({
            'phone': new FormControl(this.phone, [Validators.required, Validators.minLength(12)]),
            'cel': new FormControl(this.cel, [Validators.required, Validators.minLength(13)]),
        });
    }

    public formatCellPhone(): void {
        if (!this.phone) {
            return;
        } else {
            let formattedPhone = this.phone;
            formattedPhone = formattedPhone.replace(/\D/g, '');
            formattedPhone = formattedPhone.replace(/^(\d{2})(\d)/g, '($1) $2');
            formattedPhone = formattedPhone.replace(/(\d+)(\d{4})/, '$1-$2');
            this.phone = formattedPhone;
        }
        if (!this.cel) {
            return;
        } else {
            let formattedCellPhone = this.cel;
            formattedCellPhone = formattedCellPhone.replace(/\D/g, '');
            formattedCellPhone = formattedCellPhone.replace(/^(\d{2})(\d)/g, '($1) $2');
            formattedCellPhone = formattedCellPhone.replace(/(\d+)(\d{4})/, '$1-$2');
            this.cel = formattedCellPhone;
        }
    }

    public createSignUp(): void {
        this.store.dispatch(ProfileActions.createSignUp(
            this.store.getState().toJS().signUp.name,
            this.store.getState().toJS().signUp.cpf,
            this.store.getState().toJS().signUp.birthday,
            this.store.getState().toJS().signUp.email,
            this.phone,
            this.cel,
            this.store.getState().toJS().signUp.city,
            this.store.getState().toJS().signUp.state,
            this.store.getState().toJS().signUp.address,
            this.store.getState().toJS().signUp.number,
            this.store.getState().toJS().signUp.complement,
            this.store.getState().toJS().signUp.zipCode,
            this.store.getState().toJS().signUp.neightborhood,
            this.store.getState().toJS().signUp.mother,
            this.store.getState().toJS().signUp.password
        ));
    }

    public keytab(event): void {
        const element = event.srcElement.id;
        if (element === 'phone') {
            document.getElementById('cel').focus();
        } else if (element === 'cel' && this.step4.controls.phone.status === 'VALID'
            && this.step4.controls.cel.status === 'VALID') {
            this.createSignUp();
            this.router.navigate(['novo-cadastro/passo-5']);
        }
    }

}
