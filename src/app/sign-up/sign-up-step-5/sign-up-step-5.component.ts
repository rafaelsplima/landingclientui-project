import { Component, OnInit, Inject, AfterViewInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { AppStore } from '../../app.store';
import { ProfileActions } from '../../app.actions';
import { Router } from '@angular/router';

@Component({
    selector: 'app-sign-up-step-5',
    templateUrl: './sign-up-step-5.component.html',
    styleUrls: ['./sign-up-step-5.component.scss']
})
export class SignUpStep5Component implements OnInit, AfterViewInit {

    public step5: FormGroup;
    public city: string;
    public state: string;

    public fills = ['', '', '', '', ''];
    public blanks = ['', '', ''];

    constructor(
        @Inject(AppStore) private store,
        private router: Router,
    ) { }

    ngAfterViewInit() {
        document.getElementById('city').focus();
    }

    ngOnInit() {
        const store = this.store.getState().toJS().signUp;
        this.city = store.city;
        this.state = store.state;
        this.step5 = new FormGroup({
            'city': new FormControl(this.city, [Validators.required, Validators.minLength(3)]),
            'state': new FormControl(this.state, Validators.required),
        });
    }

    public createSignUp(): void {
        console.log(this.step5);
        if (this.step5.status === 'VALID') {
            this.store.dispatch(ProfileActions.createSignUp(
                this.store.getState().toJS().signUp.name,
                this.store.getState().toJS().signUp.cpf,
                this.store.getState().toJS().signUp.birthday,
                this.store.getState().toJS().signUp.email,
                this.store.getState().toJS().signUp.phone,
                this.store.getState().toJS().signUp.cel,
                this.city,
                this.state,
                this.store.getState().toJS().signUp.address,
                this.store.getState().toJS().signUp.number,
                this.store.getState().toJS().signUp.complement,
                this.store.getState().toJS().signUp.zipCode,
                this.store.getState().toJS().signUp.neightborhood,
                this.store.getState().toJS().signUp.mother,
                this.store.getState().toJS().signUp.password
            ));
            this.router.navigate(['/novo-cadastro/passo-6']);
        }
    }

}
