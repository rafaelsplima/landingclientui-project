import { Component, OnInit, Inject, AfterViewInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { AppStore } from '../../app.store';
import { ProfileActions } from '../../app.actions';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { MatDialog } from '@angular/material';
import { ErrorModalComponent } from '../../shared/modals/error-modal/error-modal.component';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { listLocales, defineLocale } from 'ngx-bootstrap/chronos';
import { DatePipe } from '@angular/common';
import { enGbLocale } from 'ngx-bootstrap/locale';
import { environment } from '../../../environments/environment';

@Component({
    selector: 'app-sign-up-step-2',
    templateUrl: './sign-up-step-2.component.html',
    styleUrls: ['./sign-up-step-2.component.scss']
})
export class SignUpStep2Component implements OnInit, AfterViewInit {
    public step2: FormGroup;
    public cpf: string;
    public cpfFormatted: string;
    public cpfPattern = '[0-9]{11}';
    public validCPFFlag = false;
    public birth;
    public older: boolean;

    public fills = ['', ''];
    public blanks = ['', '', '', '', '', ''];

    public locale = 'pt-br';
    public locales = listLocales();

    constructor(
        @Inject(AppStore) private store,
        public dialog: MatDialog,
        private router: Router,
        private _localeService: BsLocaleService,
        private datePipe: DatePipe,
    ) {
        enGbLocale.invalidDate = 'Data inválida';
        defineLocale(this.locale, enGbLocale);
        this._localeService.use(this.locale);
    }

    ngAfterViewInit() {
        document.getElementById('cpf').focus();
    }

    ngOnInit() {
        this.older = false;
        const store = this.store.getState().toJS().signUp;
        this.cpf = store.cpf;
        this.birth = store.birthday;
        this.step2 = new FormGroup({
            'cpf': new FormControl(this.cpf, [Validators.required, Validators.maxLength(14)]),
            'birth': new FormControl(this.birth, Validators.required),
        });
    }

    public createSignUp(): void {
        const now = moment();
        const date = moment(this.birth);
        this.formatBirthDate();
        if (!this.older) {
            this.openModalError('Declare ser maior de 18 anos');
        } else if (date.isAfter(now.subtract('18', 'years'))) {
            this.openModalError('Data de nascimento inválida');
        } else if (this.step2.valid) {
            this.store.dispatch(ProfileActions.createSignUp(
                this.store.getState().toJS().signUp.name,
                this.cpf,
                this.birth,
                this.store.getState().toJS().signUp.email,
                this.store.getState().toJS().signUp.phone,
                this.store.getState().toJS().signUp.cel,
                this.store.getState().toJS().signUp.city,
                this.store.getState().toJS().signUp.state,
                this.store.getState().toJS().signUp.address,
                this.store.getState().toJS().signUp.number,
                this.store.getState().toJS().signUp.complement,
                this.store.getState().toJS().signUp.zipCode,
                this.store.getState().toJS().signUp.neightborhood,
                this.store.getState().toJS().signUp.mother,
                this.store.getState().toJS().signUp.password
            ));
            if (environment.envName === 'beta') {
                this.router.navigate(['/novo-cadastro/passo-4']);
            } else {
                this.router.navigate(['/novo-cadastro/passo-3']);
            }
        }
    }

    public openModalError(msg: string): void {
        this.dialog.open(ErrorModalComponent, {
            data: {
                msg: msg
            },
            autoFocus: true,
            panelClass: 'sign-error-dialog',
            hasBackdrop: true,
            backdropClass: 'white-backdrop',
        });
    }

    public formatCPF(): void {
        if (!this.cpf) {
            return;
        }
        let formattedCPF = this.cpf;
        formattedCPF = formattedCPF.replace(/\D/g, '');
        formattedCPF = formattedCPF.replace(/(\d{3})(\d)/, '$1.$2');
        formattedCPF = formattedCPF.replace(/(\d{3})(\d)/, '$1.$2');
        formattedCPF = formattedCPF.replace(/(\d{3})(\d{1,2})$/, '$1-$2');
        this.cpf = formattedCPF;
    }

    public validCPF(): void {
        let Soma;
        let Resto;
        Soma = 0;

        if (!this.cpf) {
            return;
        }
        const strCPF = this.cpf.replace(/\./g, '').replace(/\-/g, '');

        if (strCPF === '00000000000') {
            this.validCPFFlag = false;
            return;
        }

        for (let i = 1; i <= 9; i++) {
            Soma = Soma + parseInt(strCPF.substring(i - 1, i), 10) * (11 - i);
        }
        Resto = (Soma * 10) % 11;

        if ((Resto === 10) || (Resto === 11)) {
            Resto = 0;
        }
        if (Resto !== parseInt(strCPF.substring(9, 10), 10)) {
            this.validCPFFlag = false;
            return;
        }

        Soma = 0;
        for (let i = 1; i <= 10; i++) {
            Soma = Soma + parseInt(strCPF.substring(i - 1, i), 10) * (12 - i);
        }
        Resto = (Soma * 10) % 11;

        if ((Resto === 10) || (Resto === 11)) {
            Resto = 0;
        }
        if (Resto !== parseInt(strCPF.substring(10, 11), 10)) {
            this.validCPFFlag = false;
            return;
        }

        this.validCPFFlag = true;
    }

    public formatBirthDate(): void {
        if (!this.birth) {
            return;
        }
        this.birth = this.datePipe.transform(this.birth, 'dd/MM/yyyy');
    }

    public invalidDate(): void {
        this.birth = '';
    }

    public keytab(event): void {
        const element = event.srcElement.id;
        if (element === 'cpf') {
            document.getElementById('birth').focus();
            const now = moment();
            const date = moment(this.birth);
        }
    }

    public maksDate(val): boolean {
        const pass = val.value;
        const expr = /[0123456789]/;

        for (let i = 0; i < pass.length; i++) {
            const lchar = val.value.charAt(i);
            const nchar = val.value.charAt(i + 1);

            if (i === 0) {
                if ((lchar.search(expr) !== 0) || (lchar > 3)) {
                    val.value = '';
                }
            } else if (i === 1) {

                if (lchar.search(expr) !== 0) {
                    const tst1 = val.value.substring(0, (i));
                    val.value = tst1;
                    continue;
                }

                if ((nchar !== '/') && (nchar !== '')) {
                    const tst1 = val.value.substring(0, (i) + 1);
                    let tst2 = '';

                    if (nchar.search(expr) !== 0) {
                        tst2 = val.value.substring(i + 2, pass.length);
                    } else {
                        tst2 = val.value.substring(i + 1, pass.length);
                    }
                    val.value = tst1 + '/' + tst2;
                }

            } else if (i === 4) {

                if (lchar.search(expr) !== 0) {
                    const tst1 = val.value.substring(0, (i));
                    val.value = tst1;
                    continue;
                }

                if ((nchar !== '/') && (nchar !== '')) {
                    const tst1 = val.value.substring(0, (i) + 1);
                    let tst2 = '';

                    if (nchar.search(expr) !== 0) {
                        tst2 = val.value.substring(i + 2, pass.length);
                    } else {
                        tst2 = val.value.substring(i + 1, pass.length);
                    }

                    val.value = tst1 + '/' + tst2;
                }
            }

            if (i >= 6) {
                if (lchar.search(expr) !== 0) {
                    const tst1 = val.value.substring(0, (i));
                    val.value = tst1;
                }
            }
        }

        if (pass.length >= 10) {
            val.value = val.value.substring(0, 9);
            return true;
        }
    }

}
