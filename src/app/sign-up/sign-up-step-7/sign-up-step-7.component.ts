import { Component, OnInit, Inject, AfterViewInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { AppStore } from '../../app.store';
import { ProfileActions } from '../../app.actions';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Auth0Config } from '../../auth/auth0.config';
import { TradeService } from '../../../api/trade.service';
import { BaseMessage } from '../../../api/messages/base-message';
import { MessageTypes } from '../../../api/messages/message-types';
import { UnauthenticatedMessage } from '../../../api/messages/unauthenticated-message';
import { SignUpMessage } from '../../../api/messages/signUp-messge';
import * as moment from 'moment';

@Component({
    selector: 'app-sign-up-step-7',
    templateUrl: './sign-up-step-7.component.html',
    styleUrls: ['./sign-up-step-7.component.scss']
})
export class SignUpStep7Component implements OnInit, AfterViewInit {

    public step7: FormGroup;
    public mother: string;

    public fills = ['', '', '', '', '', '', ''];
    public blanks = [''];

    constructor(
        @Inject(AppStore) private store,
        private router: Router,
        private http: HttpClient,
        @Inject(TradeService) private tradeService: TradeService,

    ) { }

    ngAfterViewInit() {
        document.getElementById('mother').focus();
    }

    ngOnInit() {
        const store = this.store.getState().toJS().signUp;
        this.mother = store.mother;
        this.step7 = new FormGroup({
            'mother': new FormControl(this.mother, [Validators.required, Validators.minLength(2)]),
        });
    }

    public createSignUp(): void {
        this.store.dispatch(ProfileActions.createSignUp(
            this.store.getState().toJS().signUp.name,
            this.store.getState().toJS().signUp.cpf,
            this.store.getState().toJS().signUp.birthday,
            this.store.getState().toJS().signUp.email,
            this.store.getState().toJS().signUp.phone,
            this.store.getState().toJS().signUp.cel,
            this.store.getState().toJS().signUp.city,
            this.store.getState().toJS().signUp.state,
            this.store.getState().toJS().signUp.address,
            this.store.getState().toJS().signUp.number,
            this.store.getState().toJS().signUp.complement,
            this.store.getState().toJS().signUp.zipCode,
            this.store.getState().toJS().signUp.neightborhood,
            this.mother,
            this.store.getState().toJS().signUp.password
        ));
        if (environment.envName === 'beta') {
            const cellSplit = this.store.getState().toJS().signUp.cel.split(')');
            const phoneSplit = this.store.getState().toJS().signUp.phone.split(')');
            const user = {
                client_id: environment.Auth0.clientId,
                email: this.store.getState().toJS().profile.name,
                connection: 'Username-Password-Authentication',
                user_metadata: {
                    fullname: this.store.getState().toJS().signUp.name,
                    cpf: this.store.getState().toJS().signUp.cpf,
                    mothersname: this.store.getState().toJS().signUp.mother,
                    addr_street: this.store.getState().toJS().signUp.address,
                    addr_number: this.store.getState().toJS().signUp.number,
                    addr_compl: this.store.getState().toJS().signUp.complement,
                    addr_neigh: this.store.getState().toJS().signUp.neighborhood,
                    addr_city: this.store.getState().toJS().signUp.city,
                    addr_state: this.store.getState().toJS().signUp.state,
                    addr_zip: this.store.getState().toJS().signUp.zipCode,
                    cell_areacode: cellSplit[0].replace('(', ''),
                    cell_number: cellSplit[1],
                    phone_areacode: phoneSplit[0].replace('(', ''),
                    phone_number: phoneSplit[1],
                    birthdate: moment(this.store.getState().toJS().signUp.birthday).valueOf(),
                }
            };



            const signUpMsg = new SignUpMessage(MessageTypes.UserSignUp);
            signUpMsg.Email = user.email;
            signUpMsg.CPF_CNPJ = user.user_metadata.cpf;
            signUpMsg.Broker = environment.broker.name;
            signUpMsg.Attributes = user.user_metadata;
            signUpMsg.Token = localStorage.getItem('id_token');
            this.tradeService.sendMessage(signUpMsg);
            this.router.navigate(['./novo-cadastro/sucesso']);
        }
    }

    public keytab(event): void {
        const element = event.srcElement.id;
        if (element === 'mother' && this.step7.value.mother !== '') {
            this.createSignUp();
            if (environment.envName !== 'beta') {
                this.router.navigate(['/novo-cadastro/passo-8']);
            }

        }
    }

}
