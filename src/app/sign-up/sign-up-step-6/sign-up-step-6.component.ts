import { Component, OnInit, Inject, AfterViewInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AppStore } from '../../app.store';
import { Router } from '@angular/router';
import { ProfileActions } from '../../app.actions';

@Component({
    selector: 'app-sign-up-step-6',
    templateUrl: './sign-up-step-6.component.html',
    styleUrls: ['./sign-up-step-6.component.scss']
})
export class SignUpStep6Component implements OnInit, AfterViewInit {

    public step6: FormGroup;
    public address: string;
    public number: number;
    public cep: string;
    public complement: string;
    public neightborhood: string;

    public fills = ['', '', '', '', '', ''];
    public blanks = ['', ''];

    constructor(
        @Inject(AppStore) private store,
        private router: Router,
    ) { }

    ngAfterViewInit() {
        document.getElementById('address').focus();
    }

    ngOnInit() {
        const store = this.store.getState().toJS().signUp;
        this.address = store.address;
        this.number = store.number;
        this.cep = store.zipCode;
        this.complement = store.complement;
        this.neightborhood = store.neightborhood;
        this.step6 = new FormGroup({
            'address': new FormControl(this.address, [Validators.required, Validators.minLength(3)]),
            'number': new FormControl(this.number, Validators.required),
            'cep': new FormControl(this.cep, [Validators.required, Validators.minLength(9), Validators.maxLength(9)]),
            'complement': new FormControl(this.complement),
            'neightborhood': new FormControl(this.neightborhood, [Validators.required, Validators.minLength(3)]),
        });
    }

    public formatZipCode(): void {
        if (!this.cep) {
            return;
        }
        let formattedZipCode = this.cep;
        formattedZipCode = formattedZipCode.replace(/\D/g, '');
        formattedZipCode = formattedZipCode.replace(/(\d+)(\d{3})/, '$1-$2');
        this.cep = formattedZipCode;
    }

    public createSignUp(): void {
        if (this.step6.status === 'VALID') {
            this.store.dispatch(ProfileActions.createSignUp(
                this.store.getState().toJS().signUp.name,
                this.store.getState().toJS().signUp.cpf,
                this.store.getState().toJS().signUp.birthday,
                this.store.getState().toJS().signUp.email,
                this.store.getState().toJS().signUp.phone,
                this.store.getState().toJS().signUp.cel,
                this.store.getState().toJS().signUp.city,
                this.store.getState().toJS().signUp.state,
                this.address,
                this.number,
                this.complement,
                this.cep,
                this.neightborhood,
                this.store.getState().toJS().signUp.mother,
                this.store.getState().toJS().signUp.password
            ));
            this.router.navigate(['/novo-cadastro/passo-7']);
        }
    }

    public keytab(event): void {
        const element = event.srcElement.id;
        if (element === 'address') {
            document.getElementById('number').focus();
        } else if (element === 'number') {
            document.getElementById('complement').focus();
        } else if (element === 'complement') {
            document.getElementById('cep').focus();
        } else if (element === 'cep') {
            document.getElementById('neightborhood').focus();
        } else if (element === 'neightborhood') {
            this.createSignUp();
        }
    }
}
