import { Component, OnInit, Inject, AfterViewInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { AppStore } from '../../app.store';
import { ProfileActions } from '../../app.actions';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { TermsModalComponent } from '../../shared/modals/terms-modal/terms-modal.component';
import { PrivacyModalComponent } from '../../shared/modals/privacy-modal/privacy-modal.component';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ErrorModalComponent } from '../../shared/modals/error-modal/error-modal.component';
import * as moment from 'moment';
import { TradeService } from '../../../api/trade.service';
import { SignUpMessage } from '../../../api/messages/signUp-messge';
import { MessageTypes } from '../../../api/messages/message-types';

@Component({
    selector: 'app-sign-up-step-8',
    templateUrl: './sign-up-step-8.component.html',
    styleUrls: ['./sign-up-step-8.component.scss']
})
export class SignUpStep8Component implements OnInit, AfterViewInit {

    public step8: FormGroup;
    public password: string;
    public confirmPassword: string;
    public passStrength;
    public br: boolean;
    public privateP: boolean;
    public acceptTerms: boolean;

    public fills = ['', '', '', '', '', '', '', ''];

    constructor(
        @Inject(AppStore) private store,
        private router: Router,
        private dialog: MatDialog,
        @Inject(TradeService) private tradeService: TradeService,
        private http: HttpClient,
    ) { }

    ngAfterViewInit() {
        document.getElementById('password').focus();
    }

    ngOnInit() {
        this.br = false;
        this.privateP = false;
        this.acceptTerms = false;
        const store = this.store.getState().toJS().signUp;
        this.password = store.password;
        this.confirmPassword = store.password;
        this.step8 = new FormGroup({
            'password': new FormControl(this.password, Validators.required),
            'confirmPassword': new FormControl(this.confirmPassword, Validators.required),
        });
    }

    public createSignUp(): void {
        if (this.confirmPassword !== this.password
            || this.step8.controls.password.status === 'INVALID'
            || this.step8.controls.confirmPassword.status === 'INVALID') {
            this.openModalError('Confira sua senha!');
        } else if (!this.br || !this.privateP) {
            this.openModalError('Declaração inválida!');
        } else if (!this.acceptTerms) {
            this.openModalError('Leia e aceite os termos de uso e política de privacidade!');
        } else if (this.passStrength < 40) {
            this.openModalError('Força da senha baixa!');
        } else {
            const store = this.store.getState().toJS().signUp;
            if (store.name === '' || store.cpf === '' || store.birthday === '' || store.email === ''
                || store.phone === '' || store.cel === '' || store.city === '' || store.state === ''
                || store.address === '' || store.number === '' || store.complement === ''
                || store.zipCode === '' || store.neightborhood === '' || store.mother === '') {
                this.openModalError('Preencha todos os campos!');
            } else {
                const postData = {
                    client_id: environment.Auth0.clientId,
                    email: this.store.getState().toJS().signUp.email,
                    password: this.password,
                    connection: 'Username-Password-Authentication',
                };
                const cellSplit = this.store.getState().toJS().signUp.cel.split(')');
                const phoneSplit = this.store.getState().toJS().signUp.phone.split(')');
                const user = {
                    client_id: environment.Auth0.clientId,
                    email: this.store.getState().toJS().signUp.email,
                    connection: 'Username-Password-Authentication',
                    user_metadata: {
                        fullname: this.store.getState().toJS().signUp.name,
                        cpf: this.store.getState().toJS().signUp.cpf,
                        mothersname: this.store.getState().toJS().signUp.mother,
                        addr_street: this.store.getState().toJS().signUp.address,
                        addr_number: this.store.getState().toJS().signUp.number,
                        addr_compl: this.store.getState().toJS().signUp.complement,
                        addr_neigh: this.store.getState().toJS().signUp.neighborhood,
                        addr_city: this.store.getState().toJS().signUp.city,
                        addr_state: this.store.getState().toJS().signUp.state,
                        addr_zip: this.store.getState().toJS().signUp.zipCode,
                        cell_areacode: cellSplit[0].replace('(', ''),
                        cell_number: cellSplit[1],
                        phone_areacode: phoneSplit[0].replace('(', ''),
                        phone_number: phoneSplit[1],
                        birthdate: moment(this.store.getState().toJS().signUp.birthday).valueOf(),
                    }
                };

                const signUpMsg = new SignUpMessage(MessageTypes.UserSignUp);
                signUpMsg.Email = user.email;
                signUpMsg.CPF_CNPJ = user.user_metadata.cpf;
                signUpMsg.Broker = environment.broker.name;
                signUpMsg.Attributes = user.user_metadata;

                const req = this.http.post('https://' + environment.Auth0.domain + '/dbconnections/signup', postData, {
                    headers: new HttpHeaders().set('Content-Type', 'application/json')
                });
                req.subscribe(data => {
                    signUpMsg.UserId = data['_id'];
                    this.tradeService.sendMessage(signUpMsg);
                    this.router.navigate(['./novo-cadastro/sucesso']);

                }, err => {
                });
            }
        }
    }

    public openModalError(msg: string): void {
        this.dialog.open(ErrorModalComponent, {
            data: {
                msg: msg
            },
            autoFocus: true,
            panelClass: 'sign-error-dialog',
            hasBackdrop: true,
            backdropClass: 'white-backdrop',
        });
    }

    public checkStrength(strength: number): void {
        this.passStrength = strength;
    }

    public keytab(event): void {
        const element = event.srcElement.id;
        if (element === 'password') {
            document.getElementById('confirmPassword').focus();
        }
        if (element === 'confirmPassword' && this.confirmPassword === this.password
            && this.step8.controls.password.status === 'VALID'
            && this.step8.controls.confirmPassword.status === 'VALID'
            && this.br && this.privateP && this.acceptTerms) {
            this.createSignUp();
        } else {
            this.openModalError('Confira os campos!');
        }
    }

    public openTermsDialog(): void {
        this.dialog.open(TermsModalComponent, {
            autoFocus: true,
            panelClass: 'sign-error-dialog',
            hasBackdrop: true,
            backdropClass: 'white-backdrop',
        });
    }

    public openPrivacyDialog(): void {
        this.dialog.open(PrivacyModalComponent, {
            autoFocus: true,
            panelClass: 'sign-error-dialog',
            hasBackdrop: true,
            backdropClass: 'white-backdrop',
        });
    }

}
