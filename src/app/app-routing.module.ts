// ANGULAR
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// HOME
import { HomeNotLoggedComponent } from './home-not-logged/home-not-logged.component';
import { TradingComponent } from '../app/home/trading/trading.component';
import { CandlechartComponent } from '../app/home/trading/candlechart/candlechart.component';
import { OperationsSideComponent } from '../app/home/operations-side/operations-side.component';
import { DepthchartComponent } from '../app/home/trading/depthchart/depthchart.component';
// OTHER
import { LandingpageComponent } from './landingpage/landingpage.component';
import { AboutUsComponent } from './landingpage/about-us/about-us.component';
import { JobsComponent } from './jobs/jobs.component';
import { VacancyComponent } from './jobs/vacancy/vacancy.component';
import { HowItWorksComponent } from './landingpage/how-it-works/how-it-works.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { TermsComponent } from './terms/terms.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
// NEW SIGN UP
import { SignUpComponent } from './sign-up/sign-up.component';
import { SignUpStep1Component } from './sign-up/sign-up-step-1/sign-up-step-1.component';
import { SignUpStep2Component } from './sign-up/sign-up-step-2/sign-up-step-2.component';
import { SignUpStep3Component } from './sign-up/sign-up-step-3/sign-up-step-3.component';
import { SignUpStep4Component } from './sign-up/sign-up-step-4/sign-up-step-4.component';
import { SignUpStep5Component } from './sign-up/sign-up-step-5/sign-up-step-5.component';
import { SignUpStep6Component } from './sign-up/sign-up-step-6/sign-up-step-6.component';
import { SignUpStep7Component } from './sign-up/sign-up-step-7/sign-up-step-7.component';
import { SignUpStep8Component } from './sign-up/sign-up-step-8/sign-up-step-8.component';
import { SignUpStep9Component } from './sign-up/sign-up-step-9/sign-up-step-9.component';
// PRE SIGN UP
import { PreSignUpComponent } from './pre-sign-up/pre-sign-up.component';
import { PreSignUpErrorComponent } from './pre-sign-up/pre-sign-up-error/pre-sign-up-error.component';
import { PreSignUpSuccessComponent } from './pre-sign-up/pre-sign-up-success/pre-sign-up-success.component';
import { PreSignUpVoucherComponent } from './pre-sign-up/pre-sign-up-voucher/pre-sign-up-voucher.component';
import { PreSignUpLoginComponent } from './pre-sign-up/pre-sign-up-login/pre-sign-up-login.component';
import { PreSignUpAlreadyExistsComponent } from './pre-sign-up/pre-sign-up-already-exists/pre-sign-up-already-exists.component';
import { PreSignUpFormComponent } from './pre-sign-up/pre-sign-up-form/pre-sign-up-form.component';

const appRoutes: Routes = [
    { path: '', pathMatch: 'full', component: LandingpageComponent },
    { path: 'como-funciona', component: HowItWorksComponent },
    { path: 'sobre', component: AboutUsComponent },
    { path: 'vagas', component: JobsComponent },
    { path: 'vagas/:id', component: VacancyComponent },
    { path: 'termos-de-uso', component: TermsComponent },
    { path: 'politicas-de-privacidade', component: PrivacyPolicyComponent },
    {
        path: 'exchange', component: HomeNotLoggedComponent, children: [
            { path: 'trade', component: TradingComponent },
            { path: 'candlechart', component: CandlechartComponent },
            { path: 'operations', component: OperationsSideComponent },
            { path: 'depthchart', component: DepthchartComponent},
        ]
    },
    {
        path: 'novo-cadastro', component: SignUpComponent, children: [
            { path: 'passo-1', component: SignUpStep1Component },
            { path: 'passo-2', component: SignUpStep2Component },
            { path: 'passo-3', component: SignUpStep3Component },
            { path: 'passo-4', component: SignUpStep4Component },
            { path: 'passo-5', component: SignUpStep5Component },
            { path: 'passo-6', component: SignUpStep6Component },
            { path: 'passo-7', component: SignUpStep7Component },
            { path: 'passo-8', component: SignUpStep8Component },
            { path: 'sucesso', component: SignUpStep9Component },
            { path: '**', component: PageNotFoundComponent },
        ]
    },
    {
        path: 'pre-cadastro', component: PreSignUpComponent, children: [
            { path: 'formulario', component: PreSignUpFormComponent },
            { path: 'cupom', component: PreSignUpVoucherComponent },
            { path: 'ja-existe', component: PreSignUpAlreadyExistsComponent },
            { path: 'sucesso', component: PreSignUpSuccessComponent },
            { path: 'login', component: PreSignUpLoginComponent },
            { path: 'nao-encontrado', component: PreSignUpErrorComponent },
            { path: '**', component: PageNotFoundComponent },
        ]
    },
    { path: '**', component: PageNotFoundComponent }
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [
        RouterModule
    ],
    providers: []
})

export class AppRoutingModule { }
