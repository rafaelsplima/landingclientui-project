import { environment } from '../../environments/environment';

export const Auth0Config = {
    language: 'pt-br',
    languageDictionary: {
        title: 'Abakate',
        emailInputPlaceholder: 'seu@email.com',
        passwordInputPlaceholder: 'sua senha',
    },
    auth: {
        redirectUri: environment.Auth0.redirectUri,
        audience: environment.Auth0.audience,
        responseType: environment.Auth0.responseType,
        scope: environment.Auth0.scope
    },
    theme: {
        // logo: environment.Auth0.logo,
        primaryColor: '#007ABD'
    },
    autoclose: true,
    oidcConformant: true,
};
