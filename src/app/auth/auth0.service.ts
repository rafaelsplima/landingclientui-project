import { Injectable } from '@angular/core';
import * as auth0 from 'auth0-js';
import { environment } from '../../environments/environment';


@Injectable()
export class Auth0Service {
    // Configure Auth0
    auth0 = new auth0.WebAuth({
        domain: environment.Auth0.domain,
        clientID: environment.Auth0.clientId,
        redirectUri: environment.Auth0.redirectUri,
        audience: environment.Auth0.audience,
        responseType: 'token id_token',
        scope: environment.Auth0.scope,
        prompt: 'none'
    });

    constructor() { }

    public resetPassword(email): void {
        this.auth0.changePassword({
            connection: 'Username-Password-Authentication',
            email: email,
        }, (err) => {
            if (err) {
                console.log(err);
                return;
            } else {
                console.log('Password reset link sent!');
            }
        });
    }
}
