import { Component, OnInit } from '@angular/core';
import { environment } from '../../environments/environment';

@Component({
    selector: 'app-landingpage',
    templateUrl: './landingpage.component.html',
    styleUrls: ['./landingpage.component.scss']
})
export class LandingpageComponent implements OnInit {
    public btc: boolean;

    public production;
    public navbarMobile;
    public signUpUrl;
    public signInUrl;

    constructor() {
        this.production = environment.production;
        this.signUpUrl = environment.signup_url;
        this.signInUrl = environment.signin_url;
    }


    ngOnInit() {
        this.btc = false;
    }
}
