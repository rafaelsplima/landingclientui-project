import { Injectable, Inject } from '@angular/core';
import { AppStore } from '../app/app.store';
import {
    OrderActions,
    UserActions,
    TickerActions
} from '../app/app.actions';
import { environment } from '../environments/environment';
import { setWsHeartbeat } from 'ws-heartbeat/client';

import { MatSnackBar } from '@angular/material';
import { IBaseMessage } from './messages/base-message';
import { UnauthenticatedMessage } from './messages/unauthenticated-message';
import { IDynamicMessage } from './messages/dynamic-message';
import { MessageTypes } from './messages/message-types';

enum AuthenticateState {
    NotAuthenticate,
    Authenticating,
    Authenticated,
    AuthenticationRequest
}

@Injectable()
export class TradeService {
    private idToken: string;

    public ws: WebSocket;
    public validToken = false;
    private messageQueue: Array<string> = [];
    private state: AuthenticateState;


    constructor(
        @Inject(AppStore) private store,
        public snackBar: MatSnackBar,
    ) {
        this.start(environment.ws_trade);

        store.subscribe(() => this.updateState());
    }

    start(url) {
        this.ws = new WebSocket(url);

        setWsHeartbeat(this.ws, 'h3ping', {
            pingTimeout: 500000, // in 50 seconds, if no message accepted from server, close the connection.
            pingInterval: 30000, // every 30 seconds, send a ping message to the server.
        });

        this.ws.onopen = () => {
            this.sendEnqueued();
        };
        this.ws.onclose = () => {
            if (environment.production) {
                this.store.dispatch(UserActions.logOut());
            } else {
                this.store.dispatch(UserActions.logIn());
            }
            // reconnect now
            this.validToken = false;
            console.warn('WS Closed!');
            this.check(url);
        };
        this.ws.onerror = (err) => {
            console.error(err);
            if (environment.production) {
                this.store.dispatch(UserActions.logOut());
            } else {
                this.store.dispatch(UserActions.logIn());
            }
            this.validToken = false;
            console.warn('WS Error!');
            this.check(url);
        };
        // this.ws.onmessage = this.messageReceived.bind(this);
    }

    check(url) {
        if (!this.ws || this.ws.readyState === 3) {
            this.start(url);
        }
    }

    private sendEnqueued() {
        console.log('Sending enqueued messages ', this.messageQueue);
        for (let i = 0; i < this.messageQueue.length; i++) {
            this.ws.send(this.messageQueue[i]);
            this.messageQueue.splice(i, 1);
        }
    }



    public sendMessage(msg: string | IBaseMessage): boolean {
        try {
            let message: IDynamicMessage;
            if (typeof msg === 'string') {
                message = JSON.parse(msg);
            } else {
                message = msg;
            }

            if (this.ws.readyState === WebSocket.OPEN &&
                (this.state === AuthenticateState.Authenticated ||
                    (message.MessageType === 'UserSignUp'))) {
                console.log('sending to socket ' + JSON.stringify(message));
                this.ws.send(JSON.stringify(message));
                return true;
            } else if (this.ws.readyState !== WebSocket.CONNECTING) {
                console.warn('WS is not open to send message', message);
                return false;
            } else {
                this.messageQueue.push(JSON.stringify(message));
                if (message.MessageType !== 'UserSignUp') {
                    console.log('Send logon message');
                    this.authenticate();
                }
                return true;
            }
        } catch (err) {
            console.log(this.ws.readyState);
            console.error('Error send message to socket ' + err);
        }
    }

    private updateState() {
        const state = this.store.getState();
        this.idToken = state.getIn(['auth', 'idToken']);
        if (this.state === AuthenticateState.AuthenticationRequest) {

            this.state = AuthenticateState.NotAuthenticate;
            console.log('updateState ' + this.state);
            this.authenticate();
        }

    }

    public authenticate() {
        if (
            this.idToken === '' || this.idToken === undefined || this.idToken === null
            || this.idToken === 'undefined'
        ) {
            this.validToken = false;
            this.state = AuthenticateState.AuthenticationRequest;
            console.log('authenticate ' + this.state);
        } else {
            this.validToken = true;

            if (this.state === AuthenticateState.NotAuthenticate) {
                const logonMessage = new UnauthenticatedMessage(MessageTypes.Logon);
                logonMessage.Token = this.idToken;
                logonMessage.Broker = environment.broker.name;
                console.log('sending logon message ');
                this.state = AuthenticateState.Authenticating;
                if (this.ws.readyState === WebSocket.OPEN) {
                    this.ws.send(JSON.stringify(logonMessage));
                } else {
                    console.warn('Enqueuing message', JSON.stringify(logonMessage));
                    this.messageQueue.push(JSON.stringify(logonMessage));
                }
            }
        }
    }


}
