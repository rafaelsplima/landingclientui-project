import { setWsHeartbeat } from 'ws-heartbeat/client';

export class BasicWebSocket {
    public ws: WebSocket;

    constructor(url: string) {
        this.start(url);

        setWsHeartbeat(this.ws, 'h3ping', {
            pingTimeout: 500000, // in 50 seconds, if no message accepted from server, close the connection.
            pingInterval: 30000, // every 30 seconds, send a ping message to the server.
        });
    }

    public sendMessage(message: string): boolean {
        if (this.ws.readyState === WebSocket.OPEN) {
            this.ws.send(message);
            return true;
        } else {
            console.warn('WS is not open to send message', message);
            return false;
        }
    }

    start(url) {
        this.ws = new WebSocket(url);
        this.ws.onclose = () => {
          // reconnect now
          this.check(url);
        };
      }

      check(url) {
         if (!this.ws || this.ws.readyState === 3) {
            this.start(url);
         }
      }
}
