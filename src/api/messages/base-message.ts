import { v4 as uuid } from 'uuid';
import { MessageTypes } from './message-types';

export interface IBaseMessage {
    MessageType: MessageTypes;
    MessageId: uuid;
    CreationTime: number;
    OperationId: uuid;
    OperationType: MessageTypes;
    UserId: String;
    Broker: String;
}
export class BaseMessage implements IBaseMessage {
    public MessageType: MessageTypes;
    public MessageId: uuid;
    public CreationTime: number;
    public OperationId: uuid;
    public OperationType: MessageTypes;
    public UserId: String;
    public Broker: String;


    constructor(message: string);
    constructor(messageType: MessageTypes);
    constructor(obj?: any, messageId?: uuid, creationTime?: number) {
        if (obj in MessageTypes) {
            this.MessageType = obj;
            this.MessageId = messageId || uuid();
            this.CreationTime = creationTime || Date.now();
        } else {
            const msgObj = JSON.parse(obj);
            for (const property of Object.keys(msgObj)) {
                this[property] = msgObj[property];
            }
        }

    }

}

