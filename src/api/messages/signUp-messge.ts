import { MessageTypes } from './message-types';
import { BaseMessage } from './base-message';
import { UnauthenticatedMessage } from './unauthenticated-message';


export class SignUpMessage extends UnauthenticatedMessage {
    public Email: string;
    public CPF_CNPJ: string;
    constructor(messageType: MessageTypes) {
        super(messageType);
    }
}
