import { MessageTypes } from './message-types';
import { BaseMessage } from './base-message';


export class UnauthenticatedMessage extends BaseMessage {
    public Token: string;
    public Attributes: object;
    constructor(messageType: MessageTypes) {
        super(messageType);
    }
}
