import { IBaseMessage } from './base-message';


export interface IDynamicMessage extends IBaseMessage {
    [key: string]: any;
}
